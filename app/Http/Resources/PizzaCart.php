<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PizzaCart extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'pizza_id' => $this->pizza_id,
            'cart_id' => $this->cart_id,
            'pizza_cart_id' => $this->id,
        ];
    }
}
