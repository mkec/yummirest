<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PizzaCart as PizzaCartResource;
use App\PizzaCart;

class Order extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "cart" => $this->cart_id,
            "total_pizza_price" => $this->total_pizza_price,
            "shipping" => $this->shipping,
            "total" => $this->total,
            "user_id" => $this->user_id,
            "contact_details" => $this->contact_details,
            "created_at"=> $this->created_at,
            "updated_at"=> $this->updated_at
        ];
    }
}
