<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\Order as OrderResource;
use App\PizzaCart;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return OrderResource::collection(Order::where('user_id', '=', 1)->orderBy('id', 'desc')->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($cart = Cart::find($request->cart_id)) {
            $cart->contact_details = $request->contact_details;
            $cart->save();

            $orders = new Order();
            $orders->currency = $request->currency;
            $orders->cart_id = $request->cart_id;
            $orders->shipping = $request->shipping_price;
            $orders->total_pizza_price = $request->total_pizzas_price;
            $orders->total = $request->total_price;
            $orders->user_id = $request->user_id ?? null;
            $orders->contact_details = $request->contact_details;
            $orders->save();

            return response()->json([
                "status" => "201",
                "message" => "Resource created!"
            ]);
        } else {
            return response()->json([
                "status" => "404",
                "message" => "Cart not found!"
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($order)
    {
        if ($this->isJson($order)) {
            //return Order::where('contact_details', '=', $order)->get();
        }
        return OrderResource::collection(Order::where('user_id', '=', $order)->paginate());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    private function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
