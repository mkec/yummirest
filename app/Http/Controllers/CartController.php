<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use App\Http\Resources\Cart as CartResource;
use App\PizzaCart;
use ErrorException;
use Exception;
use Throwable;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cart_id = $request->cart_id;
        if (is_null($cart_id)) {
            $cart = new Cart();
            if ($cart->save()) {
                $cart_id = $cart->id;
            }
        }

        $pizza_cart_check = PizzaCart::where('cart_id', '=', $cart_id)->where('pizza_id', '=', $request->id)->first();
        if (is_null($pizza_cart_check)) {
            $pizza_cart_check = new PizzaCart();
            $pizza_cart_check->cart_id = $cart_id;
            $pizza_cart_check->pizza_id = $request->id;
            $pizza_cart_check->quantity = json_encode($request->quantity, true);
            $pizza_cart_check->save();
        } else {
            $pizza_cart_check->quantity = json_encode($request->quantity, true);
            $pizza_cart_check->save();
        }

        return response()->json([
            'id' => $pizza_cart_check->pizza_id,
            'cart_id' => $pizza_cart_check->cart_id,
            'quantity' => $pizza_cart_check->quantity
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show($cart)
    {
        if ($cartFound = Cart::with('pizzas')->find($cart)) {
            return new CartResource(Cart::with('pizzas')->find($cart));
        }
        return response()->json([
            "status" => "404",
            "message" => "Cart not found!"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy($cart, $pizza)
    {
        $pizza_cart_check = PizzaCart::where('pizza_id', '=', $pizza)->where('cart_id', '=', $cart)->first();

        if (!is_null($pizza_cart_check)) {
            $pizza_cart_check->delete();
            return response()->json([
                "status" => "204",
                "message" => "Deleted successfully!"
            ]);
        }
        return response()->json([
            "status" => "404",
            "message" => "Cart 4 Pizza not found!"
        ]);
    }
}
