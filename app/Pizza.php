<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    //protected $fillable = ['title', 'description', 'price', 'img'];

    /**
     * Get
     */
    public function carts()
    {
        return $this->belongsToMany('App\Cart', 'pizza_carts')->as('pizza_carts')->withPivot('id', 'quantity');
    }
}
