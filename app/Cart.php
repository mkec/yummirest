<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['user_id', 'contact_details'];

    public function pizzas()
    {
        return $this->belongsToMany('App\Pizza', 'pizza_carts')->as('pizza_carts')->withPivot('id', 'quantity');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected $attributes = [
        'contact_details' => '{}'
    ];
}
