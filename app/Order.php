<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    protected $fillable = ['pizza_cart_id', 'total_pizza_price', 'shipping', 'total', 'user_id', 'contact_details'];

    /**
     * Get the pizzacarts for the carts.
     */
    public function pizzacarts()
    {
        return $this->belongsTo('App\PizzaCart');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
