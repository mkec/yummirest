<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PizzaCart extends Model
{
    protected $fillable = ['id','cart_id', 'pizza_id', 'quantity'];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    protected $attributes = [
        'quantity' => '{}'
    ];
}
