<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pizza_cart_id');
            $table->double('total_pizza_price', 8, 2);
            $table->double('shipping', 8, 2);
            $table->double('total', 8, 2);
            $table->unsignedBigInteger('user_id')->nullable();
            $table->json('contact_details');
            $table->timestamps();
            $table->foreign('pizza_cart_id')
                    ->references('id')->on('pizza_carts');
            $table->foreign('user_id')
                    ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
