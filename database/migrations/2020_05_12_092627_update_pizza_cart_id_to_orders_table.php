<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePizzaCartIdToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['pizza_cart_id']);
            $table->dropColumn('pizza_cart_id');
            $table->unsignedBigInteger('cart_id');
            $table->foreign('cart_id')
                    ->references('id')->on('carts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['cart_id']);
            $table->dropColumn('cart_id');
            $table->unsignedBigInteger('pizza_cart_id');
            $table->foreign('pizza_cart_id')
                    ->references('id')->on('pizza_carts')->onDelete('cascade');
        });
    }
}
