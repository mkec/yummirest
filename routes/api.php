<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::delete('cart/{cart}/pizza/{pizza}', [
    'as' => 'cart.destroy', 'uses' => 'CartController@destroy']);

Route::resources([
    'pizzas' => 'PizzaController',
    'cart' => 'CartController',
    'orders' => 'OrderController'
    ]);

    Auth::routes();
